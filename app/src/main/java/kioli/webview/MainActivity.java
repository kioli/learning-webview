package kioli.webview;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.webkit.ConsoleMessage;
import android.webkit.CookieManager;
import android.webkit.DownloadListener;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.TextView;
import android.widget.Toast;


public class MainActivity extends Activity implements WebPageLoadListener {

    private String url;
    private WebView webview;
    private TextView header;
    private WebSettingsHelper webSettingsHelper;
    private MyWebClient webViewClient;
    private CookieManager cookieManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        webview = (WebView) findViewById(R.id.webview);
        header = (TextView) findViewById(R.id.header);
        init();
    }

    private void init() {
        webSettingsHelper = WebSettingsHelper.getInstance(this);
        updateSettings();

        webview.setWebChromeClient(new WebChromeClient() {
            @Override
            public boolean onConsoleMessage(ConsoleMessage consoleMessage) {
                return true;
            }
        });

        final WebNativeInterface webNativeInterface = new WebNativeInterface();

        webViewClient = new MyWebClient(this, webSettingsHelper, webNativeInterface);

        // FILL WITH REAL DATA IF NECESSARY
        webViewClient.setUsernamePassword("username", "password");

        webview.setWebViewClient(webViewClient);

        webview.setDownloadListener(new DownloadListener() {
            @Override
            public void onDownloadStart(final String url, final String userAgent, final String contentDisposition, final String mimetype, final long contentLength) {
                // TODO CHECK ONLY IF NECESSARY
            }
        });

        cookieManager = CookieManager.getInstance();
        cookieManager.setAcceptCookie(true);

        final String protocol = "HTTPS_PRIVATE";
        final String host = "www.google.com";

        url = protocol + "://" + host;
        url = "file:///android_asset/mockwidget.html";

        // TODO IN CASE I HAVE SOME
        // cookieManager.setCookie(host, cookie.toHeaderValue());

        // TODO IN CASE I HAVE SOME
        // webview.getSettings().setUserAgentString("my user agent");

        webviewGoesToPage(url);
    }

    private void webviewGoesToPage(String url) {
        if (url != null) {
            webview.loadUrl(url);
        }
    }

    private void updateSettings() {
        WebSettings webSettings = webview.getSettings();
        webSettings.setJavaScriptEnabled(webSettingsHelper.isJavascriptEnabled());
        webSettings.setBuiltInZoomControls(webSettingsHelper.isBuiltInZoomControls());

        //We need to allow universal access for file scheme URIs as we are currently using a local html file.
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.JELLY_BEAN) {
            webSettings.setAllowUniversalAccessFromFileURLs(true);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            webSettings.setDisplayZoomControls(webSettingsHelper.isDisplayZoomControls());
        }
    }

    @Override
    public void onSaveInstanceState(final Bundle outState) {
        super.onSaveInstanceState(outState);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            cookieManager.removeAllCookies(null);
        } else {
            cookieManager.removeAllCookie();
        }

        webview.clearCache(true);
    }

    @Override
    public void onBackPressed() {
        webview.loadUrl("javascript: backButtonPressed();");

        if (webview.canGoBack()) {
            webview.stopLoading();
            webview.goBack();
        } else {
            webview.stopLoading();
            finish();
        }
    }

    @Override
    public void onPageStartLoading() {
        // SHOW LOADING SPINNER MAYBE
    }

    @Override
    public void onPageLoadedSuccessfully() {
        header.setText(webview.getTitle());

        webview.loadUrl("javascript: var script = document.createElement('script');" +
                "script.type = 'text/javascript';" +
                "script.text = '" +
                "var webNativeInterface = new Object();" +
                "webNativeInterface.showToast = function() { var req = new XMLHttpRequest(); req.open(\"GET\",\"https://web2app/toast\"); req.send(null); };" +
                "webNativeInterface.goToScreen = function(url) { var req = new XMLHttpRequest(); req.open(\"GET\",\"https://web2app/gotoscreen\".concat(\"/\").concat(url)); req.send(null); };" +
                "webNativeInterface.goBack = function() { var req = new XMLHttpRequest(); req.open(\"GET\",\"https://web2app/goback\"); req.send(null); };" +
                "webNativeInterface.setTitle = function(title) { var req = new git remote add origin git@bitbucket.org:kioli/learning-webview.git(); req.open(\"GET\",\"https://web2app/title\".concat(\"/\").concat(title)); req.send(null); };" +
                "webNativeInterface.createLogger = function() { var req = new XMLHttpRequest(); req.open(\"GET\",\"https://web2app/logger\"); req.send(null); }';" +
                "document.getElementsByTagName('head')[0].appendChild(script);");

        // HIDE LOADING SPINNER MAYBE
    }

    @Override
    public void onPageLoadedFailure(int errorCode, String description, String failingUrl) {
        // SHOW ERROR PAGE
    }

    @Override
    public void loadPageExternal(Uri uri) {
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        startActivity(intent);
    }

    private class WebNativeInterface implements WebClientCallback {

        Toast toast;

        public void showToast(final String toastMessage) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (toast != null) {
                        toast.cancel();
                    }
                    toast = Toast.makeText(MainActivity.this, toastMessage, Toast.LENGTH_LONG);
                    toast.show();
                }
            });
        }

        public void setTitle(final String title) {
            //Set the title.
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    header.setText(title);
                }
            });
        }

        public void createLogger(final String tag, final String level, final String message) {
            try {
                Log.println(Log.class.getField(level).getInt(Log.class), tag, message);
            } catch (NoSuchFieldException n) {
                Log.e(tag, "NoSuchFieldException: " + n.getMessage());
            } catch (IllegalAccessException i) {
                Log.e(tag, "IllegalAccessException: " + i.getMessage());
            }
        }

        public void goBack() {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    onBackPressed();
                }
            });
        }

        public void goToScreen(final String url) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    webviewGoesToPage(url);
                }
            });
        }
    }
}
