package kioli.webview;

import android.net.Uri;

public interface  WebPageLoadListener {

    /**
     * Called when a page starts loading
     */
    void onPageStartLoading();

    /**
     * Called when a page has finished loading successfully
     */
    void onPageLoadedSuccessfully();

    /**
     * Called when a page has finished loading with failure
     *
     * @param errorCode
     * @param description
     * @param failingUrl
     */
    void onPageLoadedFailure(int errorCode, String description, String failingUrl);


    /**
     * Load the page externally.
     *
     * @param uri
     */
    void loadPageExternal(Uri uri);

}
