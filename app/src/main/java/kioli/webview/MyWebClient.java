package kioli.webview;

import android.annotation.TargetApi;
import android.graphics.Bitmap;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Build;
import android.util.Log;
import android.webkit.HttpAuthHandler;
import android.webkit.SslErrorHandler;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import java.lang.ref.WeakReference;

public class MyWebClient extends WebViewClient {

    private static final String TAG = "WebClient";

    private final WeakReference<WebPageLoadListener> webPageLoadListenerWeakReference;
    private WebSettingsHelper webSettingsHelper;
    private final WeakReference<WebClientCallback> webViewClientCallbacksWeakReference;

    private String loadUrl;
    private boolean loading = false;
    private String username, password;

    public MyWebClient(WebPageLoadListener webPageLoadListener, WebSettingsHelper webSettingsHelper, WebClientCallback webViewClientCallbacks) {
        this.webPageLoadListenerWeakReference = new WeakReference<>(webPageLoadListener);
        this.webSettingsHelper = webSettingsHelper;
        this.webViewClientCallbacksWeakReference = new WeakReference<>(webViewClientCallbacks);
    }

    @Override
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public WebResourceResponse shouldInterceptRequest(WebView view, WebResourceRequest request) {
        //TODO: This should be properly tested on an emulator running API level 21 or higher.
        return shouldInterceptRequest(view, request.getUrl().toString());
    }

    @Override
    public WebResourceResponse shouldInterceptRequest(WebView view, String url) {
        Uri uri = Uri.parse(Uri.decode(url));

        String basePath = null;
        if (uri.getPathSegments().size() >= 1) {
            basePath = uri.getPathSegments().get(0);
        }

        String parameter = null;
        if (uri.getPathSegments().size() >= 2) {
            parameter = uri.getPathSegments().get(1);
        }

        WebClientCallback webViewClientCallbacks = webViewClientCallbacksWeakReference.get();

        if (webViewClientCallbacks != null) {
            if (basePath != null) {
                if (basePath.equalsIgnoreCase("toast")) {
                    webViewClientCallbacks.showToast("Some toast message");
                } else if (basePath.equalsIgnoreCase("logger")) {
                    webViewClientCallbacks.createLogger(TAG, "DEBUG", "Some log message.");
                } else if (basePath.equalsIgnoreCase("title")) {
                    webViewClientCallbacks.setTitle(parameter);
                } else if (basePath.equalsIgnoreCase("goback")) {
                    webViewClientCallbacks.goBack();
                } else if (basePath.equalsIgnoreCase("gotoscreen")) {
                    webViewClientCallbacks.goToScreen(parameter);
                }
            }
        }
        return super.shouldInterceptRequest(view, url);
    }

    @Override
    public boolean shouldOverrideUrlLoading(WebView view, String url) {
        Uri uri = Uri.parse(url);
        Uri loadedUri = Uri.parse(loadUrl);

        boolean openExternal = webSettingsHelper.isOpenOtherHostLinks();
        boolean openSameHost = webSettingsHelper.isOpenSameHostLinks();

        if (!openExternal || !openSameHost) {
            try {
                final WebPageLoadListener webPageLoadListener = webPageLoadListenerWeakReference.get();
                if (webPageLoadListener != null) {

                    String loadedUriHost = loadedUri.getHost();
                    String currentUriHost = uri.getHost();

                    Log.d(TAG, "loadedUriHost: " + loadedUriHost + " currentUriHost: " + currentUriHost);

                    if (loadedUriHost.equals(currentUriHost)) {
                        if (!openSameHost) {
                            webPageLoadListener.loadPageExternal(uri);
                            return true;
                        }
                    } else {
                        if (!openExternal) {
                            webPageLoadListener.loadPageExternal(uri);
                            return true;
                        }
                    }
                }

            } catch (final NullPointerException e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    @Override
    public void onPageStarted(WebView view, String url, Bitmap favicon) {
        // update stored url, in case of redirection
        loadUrl = url;
        loading = true;

        final WebPageLoadListener webPageLoadListener = webPageLoadListenerWeakReference.get();
        if (webPageLoadListener != null) {
            webPageLoadListener.onPageStartLoading();
        }
    }

    @Override
    public void onPageFinished(WebView view, String url) {
        if (loading) {
            final WebPageLoadListener webPageLoadListener = webPageLoadListenerWeakReference.get();
            if (webPageLoadListener != null) {
                webPageLoadListener.onPageLoadedSuccessfully();
            }
        }

        loading = false;
    }

    @Override
    public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
        loading = false;

        final WebPageLoadListener webPageLoadListener = webPageLoadListenerWeakReference.get();
        if (webPageLoadListener != null) {
            webPageLoadListener.onPageLoadedFailure(errorCode, description, failingUrl);
        }
    }

    @Override
    public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
        if (webSettingsHelper.isIgnoreSslError()) {
            handler.proceed();
        } else {
            handler.cancel();
        }
    }

    @Override
    public void onReceivedHttpAuthRequest(WebView view, HttpAuthHandler handler, String host, String realm) {
        if (username != null && password != null) {
            handler.proceed(username, password);
        } else {
            handler.cancel();
        }
    }

    @Override
    public void onReceivedLoginRequest(WebView view, String realm, String account, String args) {
        //TODO implement this method if necessary.
    }

    public void setUsernamePassword(String username, String password) {
        this.username = username;
        this.password = password;
    }
}
