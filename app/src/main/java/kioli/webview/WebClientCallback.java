package kioli.webview;

public interface WebClientCallback {
    void showToast(final String toastMessage);

    void setTitle(final String title);

    void createLogger(final String tag, final String level, final String message);

    void goBack();

    void goToScreen(final String url);
}
